.. pytolstack documentation master file, created by
   sphinx-quickstart on Wed May  1 10:23:35 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pytolstack Documentation
======================================
.. toctree::
   :maxdepth: 3
   :caption: Contents:


ToleranceStack
===================
.. autoclass:: pytolstack.ToleranceStack
   :members:


Assembly
===================
.. autoclass:: pytolstack.Assembly
   :members:
   :show-inheritance:


Component
===================
.. autoclass:: pytolstack.Component
   :members:


Feature
===================
.. autoclass:: pytolstack.Feature
   :members:


Dimension
===================
.. autoclass:: pytolstack.Dimension
   :members:
   :undoc-members:
   :show-inheritance:


Tolerance
===================
.. autoclass:: pytolstack.Tolerance
   :members:


GTol
===================
.. autoclass:: pytolstack.GTol
   :members:
   :undoc-members:
   :show-inheritance:


GTolType
===================
.. autoclass:: pytolstack.GTolType
   :members:
   :undoc-members:


TolStackObject
===================
.. autoclass:: pytolstack.TolStackObject
   :members:


.. Global
.. ===================
.. .. automodule:: pytolstack
..    :members: random_hex


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
