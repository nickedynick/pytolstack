import pytolstack as pts

def test_dimension():
	mean = 0
	tol = 1
	lim = 0.01
	dp = 3

	dim = pts.Dimension(mean, pts.Tolerance(tol))
	res = dim + dim

	assert round((1+lim)*mean, dp) >= round(res.mean, dp), "mean too high"
	assert round(res.mean, dp) >= round((1-lim)*mean, dp), "mean too low"

	assert round((1+lim)*2*tol/3, dp) >= round(res.std, dp), "standard deviation too high"
	assert round(res.std, dp) >= round((1-lim)*2*tol/3, dp), "standard deviation too low"
