import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

exec(open('pytolstack/version.py').read())

setuptools.setup(
    name="pytolstack",
    version=__version__,
    author="Nick Stringer",
    author_email="nickedynick@dgmail.com",
    description="A package for carrying out design tolerance stacks",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://nickstringer.dev",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
