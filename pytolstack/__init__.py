from .version import __version__

import pandas as pd
import numpy as np

import math
import scipy.stats as ss

import mcerp
import mcerp.umath as umath

from enum import Enum

import random

import matplotlib.pyplot as plt

import seaborn as sns

import plotly.offline as ply
import plotly.graph_objs as go

mcerp.npts = int(1e5)


'''
    ToDo:
     - Create Feature class to sit between Component and Dimension?
     - Summary Report function?
     - A/B tolerance stack comparison
        - May need to have StackComparison class, ToleranceStack.copy()
     - Logistic Regression (or similar) for reverse calculation, etc.
'''

class Tolerance(object):
    def __init__(self, upper, lower=None):
        if lower is not None:
            self.tolerance_type = 'bilateral'
            if lower > 0: lower = -lower
            self.lower = lower
            self.upper = upper
        else:
            if upper == 0:
                upper = 1e-12
            self.tolerance_type = 'symmetric'
            self.lower = -upper
            self.upper = upper


class DimensionType(Enum):
    UNKNOWN = 0
    TED = 1
    MIN = 1
    MAX = 1


class Dimension(mcerp.UncertainVariable):
    def __init__(self, nominal, tolerance=Tolerance(0), special=None, distribution='Normal', tag=None, unit='mm'):        
        self.nominal = nominal
        self.tolerance = tolerance

        self.unit=unit
        
        feature_types = {
            'CI': 1,
            'QC': 5/3,
            'SC': 5/3,
            'FF': 5/3
        }
        
        if special in feature_types:
            self.cpk = feature_types[special]
        else:
            self.cpk = 1
        
        self.min = self.nominal + self.tolerance.lower
        self.max = self.nominal + self.tolerance.upper
        self.med = (self.max + self.min) / 2
        
        if distribution == 'Normal':
            mcerp.UncertainVariable.__init__(self, ss.norm(loc=self.med, scale=self.stddev), tag=tag)
        elif self.distribution == 'Uniform':
            mcerp.UncertainVariable.__init__(self, ss.uniform(loc=self.min, scale=self.max - self.min), tag=tag)
    
    
#     @classmethod
#     def fromUncertaintyVariable(self, uv, special=None):
#         self.nominal = uv.mean
#         stddev = math.sqrt(uv.var)
#         self.tolerance = Tolerance(3*stddev)
#         self.tag = uv.tag
        
#         feature_types = {
#             'CI': 1,
#             'QC': 5/3,
#             'SC': 5/3,
#             'FF': 5/3
#         }
        
#         if special is None:
#             self.cpk = 1
#         else:
#             self.cpk = feature_types[special]
        
#         self.min = self.nominal + self.tolerance.lower
#         self.max = self.nominal + self.tolerance.upper
        
#         self._mcpts = uv._mcpts
    

    @property
    def n_sigma(self):
        return 3 * self.cpk
    
    
    @property
    def mean(self):
        return 0.5 * ((self.nominal + self.tolerance.lower) + (self.nominal + self.tolerance.upper))
    
    
    @property
    def stddev(self):
        min_tol = (self.tolerance.upper - self.tolerance.lower) / 2
        return min_tol / self.n_sigma
    
    
    @property
    def USL(self):
        return self.mean + self.n_sigma * self.stddev
    
    
    @property
    def LSL(self):
        return self.mean - self.n_sigma * self.stddev
    
    
    @property
    def range(self):
        return self.USL - self.LSL
    
    
    def __str__(self):
        if self.tolerance.type == 'symmetric':
            return '{:.3f} ±{:.3f} {}'.format(self.nominal, self.tolerance.upper, self.unit)
        else:
            return '{:.3f} +{:.3f}/{:.3f} {}'.format(self.nominal, self.tolerance.upper, self.tolerance.lower, self.unit)
    
    
    def plot(self):
        title = 'Dimension Distribution' if self.tag is None else self.tag
        plot_title = '{}\n{:.3f} ±{:.3f} ({}σ)'.format(title, self.mean, self.range / 2, self.n_sigma)

        f, ax_hist = plt.subplots(
            figsize=(9, 5)
        )

        sns.distplot(
            self._mcpts,
            ax=ax_hist
        )

        ax_hist.set(
            xlabel='Resultant Dimension [{}]'.format(self.unit),
            ylabel='Frequency',
            title=(plot_title)
        )
        
        y_lims = ax_hist.get_ylim()
        ax_hist.set_ylim(y_lims[0], y_lims[1] * 1.05)
        
        ax_hist.axvline(self.LSL, color='k', linestyle='dashed', linewidth=1)
        ax_hist.axvline(self.mean, color='k', linestyle='dashed', linewidth=1)
        ax_hist.axvline(self.USL, color='k', linestyle='dashed', linewidth=1)
        
        offset = 0.015*self.range
        y_pos = 0.95 * ax_hist.get_ylim()[1]
        
        ax_hist.text(self.LSL + offset, y_pos, 'LSL')
        ax_hist.text(self.mean + offset, y_pos, 'Mean')
        ax_hist.text(self.USL + offset, y_pos, 'USL')
        
    
    def tolerance_range(self):
        lsl = self.mean - self.n_sigma * self.stddev
        usl = self.mean + self.n_sigma * self.stddev

        return lsl, usl, self.n_sigma
    
    
    def describe_tolerance_range(self):        
        print('Tolerance range ({}σ): {:.3f} ±{:.3f}'.format(self.n_sigma, self.mean, self.range / 2))
        
        print('{:.2%} of values below LSL ({:.3f})'.format(self < self.LSL, self.LSL))
        print('{:.2%} of values above USL ({:.3f})'.format(self > self.USL, self.USL))


    def print_ppm(self, limit, under=True):
        if under:
            direction = 'below'
            result = self < limit
        else:
            direction = 'above'
            result = self > limit
        
        print('{:.2f} ppm {} {:.3f} {}'.format((result)*1e6, direction, limit, self.unit))


class GTolType(Enum):
    UNKNOWN = 0
    STRAIGHTNESS = 1
    FLATNESS = 2
    ROUNDNESS = 3
    CYLINDRICITY = 4
    PROFILE_LINE = 5
    PROFILE_SURFACE = 6
    PARALLELISM = 7
    PERPENDICULARITY = 8
    ANGULARITY = 9
    POSITION = 10
    CONCENTRICITY = 11
    COAXIALITY = 11
    SYMMETRY = 12
    RUNOUT = 13
    RUNOUT_TOTAL = 14


class GTol(Dimension):
    def __init__(self, value, special=None, distribution='Normal', tag=None, gtoltype=GTolType.UNKNOWN):
        tolerance = Tolerance(value/2)
        nominal = 0

        self.gtoltype = gtoltype
        
        Dimension.__init__(self, nominal, tolerance, special, distribution, tag)


class ResultantDimension(Dimension):
    def __init__(self, uv, special=None, distribution='Normal'):        
        nominal = uv.mean
        stddev = math.sqrt(uv.var)
        tol = Tolerance(3*stddev)
        
        Dimension.__init__(self, nominal, tol, special, distribution, 'Result - {}'.format(uv.tag))
        
        self._mcpts = uv._mcpts


class Component(object):
    def __init__(self, name, number=None, rev=None):
        self.name = name
        self.number = number
        self.rev = rev
        self.dimensions = {}
    
    def __setitem__(self, name, dim):
        dim.tag = '{} - {}'.format(self.name, name)
        self.dimensions[name] = dim 
    
    def __getitem__(self, key):
        return self.dimensions[key]


class ToleranceStack(object):
    def __init__(self, components, results, tag=None):
        self.components = components
        self.results = results
        self.tag = tag
    
    
    def get_feature_names(self):
        dims = []
        results = []
        
        stack = self.results.copy()
        stack.extend(self.components)

        for item in stack:
            if type(item) is Component:
                for dim in item.dimensions:
                    dims.append('{} - {}'.format(item.name, dim))
            elif type(item) is ResultantDimension:
                results.append(item.tag)
        
        return dims, results
        
    
    def generate_data_frame(self):
        df_all = pd.DataFrame()
        
        stack = self.results.copy()
        stack.extend(self.components)
        
        for component in stack:
            if type(component) is Component:
                dims = [component.dimensions[dim]._mcpts for dim in component.dimensions]
                names = [component.dimensions[dim].tag for dim in component.dimensions]
                
                df_component = pd.DataFrame(dims).transpose()
                df_component.columns = names
                
                df_all = pd.concat([df_all, df_component], axis=1, sort=False)
            elif type(component) is ResultantDimension:
                df_resultant = pd.DataFrame(component._mcpts)
                df_resultant.columns = [component.tag]
                
                df_all = pd.concat([df_all, df_resultant], axis=1, sort=False)
        
        self.data_frame = df_all
    
    
    def most_correlated(self, n=10, i_result=0):
        if not hasattr(self, 'data_frame'):
            self.generate_data_frame()
        
        dims, results = self.get_feature_names()
        
        corr = self.data_frame.corr().loc[dims, results].take([i_result], axis=1)
        corr.columns = ['Correlation']
        corr['Abs'] = corr['Correlation'].abs()

        corr.sort_values(by='Abs', ascending=False, inplace=True)
        
        return corr[['Correlation']].head(n)
    
    
    def plot_correlation(self, n=None, absolute=False, results_only=False):
        if not hasattr(self, 'data_frame'):
            self.generate_data_frame()
        
        corr = self.data_frame.corr()
        
        dims, results = self.get_feature_names()
        
        if results_only:
            corr = corr.loc[results, dims]
            mask = None
            
            plot_title = 'Results Correlation Matrix'
        else:
            mask = np.zeros_like(corr, dtype=np.bool)
            mask[np.triu_indices_from(mask)] = True
            
            plot_title = 'Full Correlation Matrix'
        
        if n is not None:
            most_correlated = self.most_correlated(n).index.tolist()
            
            if results_only:
                corr = corr.loc[results, most_correlated]
                mask = None
            else:
                cols = [r for r in results]
                cols.extend(most_correlated)
                corr = corr.loc[cols, cols]
                
                mask = np.zeros_like(corr, dtype=np.bool)
                mask[np.triu_indices_from(mask)] = True
                        
            plot_title = 'Correlation Matrix - Top {}'.format(n)
        
        if absolute:
            corr = corr.abs()
            plot_title = plot_title + '\n(absolute values)'
        
        # Set up the matplotlib figure
        f, ax = plt.subplots(figsize=(15, 10))
        
        # Generate a custom diverging colormap
        cmap = sns.diverging_palette(220, 10, as_cmap=True)

        # Draw the heatmap with the mask and correct aspect ratio
        ax = sns.heatmap(
            corr, cmap=cmap, mask=mask,
            center=0, vmax=1, vmin=0 if absolute else -1,
            square=True, linewidths=.5, cbar_kws={"shrink": .5},
#             annot=True
        )

        ax.set_title(plot_title)

